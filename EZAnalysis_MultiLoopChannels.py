import numpy as np
import LTATools as lta
import matplotlib.pyplot as plt
import pickle
import os
import argparse as arg
import pandas as pd

import re
import requests
import sys

from datetime import datetime, timezone
from pydantic import BaseModel, Field
from typing import List, Tuple

# ----------------------------------------
# Assorted constants
VERSION = 'EZAnalysis_MultiScan_1.3.0'
FIG_SIZE = (11, 5.5)

FOLDER_PLOT = 'AnalysisTimeChannel'
FOLDER_BARS = 'AnalysisBars'

DS_URL = "http://ec2-52-15-76-57.us-east-2.compute.amazonaws.com:8000"

# ----------------------------------------
# Arg parser
helpline = 'This tools processes EZTime time series data. '\
    'It will produce time series plots and corresponding bar charts. '\
    'Also can prepare data for upload.'
parser = arg.ArgumentParser(description=helpline)

parser.add_argument('-f', nargs='+', help='EZRUN folders to analyze')
parser.add_argument('-a', help='Analysis Destination. Default dir = .\\')
parser.add_argument('-p', action='store_true', help='Push data to NAS')

args = parser.parse_args()


# ----------------------------------------
"""EzTime CSV to MongoDB populating script. """


class EzTimeIteration(BaseModel):
    timestamp: datetime = Field(...)
    iteration: int = Field(..., ge=0)
    dosing: float = Field(...)
    oxygen: float = Field(...)
    glucose: float = Field(...)
    A: float = Field(..., title="Amplitude")
    tau: float = Field(..., title="Lifetime")
    B: float = Field(..., title="Linear component")
    C: float = Field(..., title="Offset")
    residue: float = Field(...)


class EzTimeSubsensor(BaseModel):
    name: str = Field(..., regex="^[A-H]$")
    iterations: List[EzTimeIteration] = Field(...)


class EzTimeChannel(BaseModel):
    name: str = Field(..., regex="^(Hpo|Eug|Hpr)$")
    subsensors: List[EzTimeSubsensor] = Field(...)


class EzTimeRecord(BaseModel):
    test_id: str = Field(...)
    card_id: str = Field(...)
    sensor_pos: int = Field(..., ge=1, le=18)
    run_id: str = Field(...)
    card_tag: str = Field(...)
    sensor_tag: str = Field(...)
    timestamp: datetime = Field(...)
    channels: List[EzTimeChannel] = Field(...)


def get_metadata(csv_file: str) -> Tuple[str, datetime]:
    """Retrieve metadata from CSV name"""
    run_id_naming = re.compile(r".*(EZRUN_(\d+)).*\.csv$")
    m = run_id_naming.match(os.path.basename(csv_file))

    if m is None:
        raise ValueError(f"{csv_file} has no Run ID")

    run_id = m.group(1)
    timestamp = datetime.strptime(m.group(2), "%Y%m%d%H%M").replace(
        tzinfo=timezone.utc
    )

    return run_id, timestamp


def eztime_csv_to_df(csv_file: str) -> pd.DataFrame:
    """Convert an EzTime CSV to a DataFrame"""
    df = pd.read_csv(csv_file)
    df.rename(
        errors="raise",
        inplace=True,
        columns={
            "ID_Test": "test_id",
            "Card": "card_id",
            "Sensor N": "sensor_pos",
            "Subsensor": "subsensor",
            "Ch": "channel",
            "tag_sensor": "sensor_tag",
            "tag_card": "card_tag",
            "I": "iteration",
            "Dosing": "dosing",
            "nomO2%": "oxygen",
            "nomGlu": "glucose",
            "Timestamp": "timestamp",
            "A1": "A",
            "t1": "tau",
            "Res": "residue",
        },
    )

    return df


def eztime_df_to_db(
    df: pd.DataFrame, run_id: str, record_timestamp, url: str
) -> None:
    """Post an EzTime DataFrame to the url DB gateway"""
    sensor_ids = df.groupby(["card_id", "sensor_pos"]).indices.keys()
    for sensor_id in sensor_ids:
        card_id = sensor_id[0]
        sensor_pos = sensor_id[1]
        df_sensor = df.loc[
            (df["card_id"] == card_id) & (df["sensor_pos"] == sensor_pos)
        ]
        test_id = df_sensor.iloc[0]["test_id"]
        card_tag = df_sensor.iloc[0]["card_tag"]
        sensor_tag = df_sensor.iloc[0]["sensor_tag"]
        channels: List[EzTimeChannel] = []
        channel_ids = df_sensor.groupby(["channel"]).indices.keys()
        for channel in channel_ids:
            df_channel = df_sensor.loc[df_sensor["channel"] == channel]
            subsensors: List[EzTimeSubsensor] = []
            subsensor_ids = df_channel.groupby(["subsensor"]).indices.keys()
            for subsensor in subsensor_ids:
                df_subsensor = df_channel.loc[
                    df_channel["subsensor"] == subsensor
                ]
                iterations: List[EzTimeIteration] = []
                for row in df_subsensor.itertuples():
                    timestamp = datetime.fromtimestamp(row.timestamp).replace(
                        tzinfo=timezone.utc
                    )
                    iterations.append(
                        EzTimeIteration(
                            timestamp=timestamp,
                            iteration=row.iteration,
                            dosing=row.dosing,
                            oxygen=row.oxygen,
                            glucose=row.glucose,
                            A=row.A,
                            tau=row.tau,
                            B=row.B,
                            C=row.C,
                            residue=row.residue,
                        )
                    )
                subsensors.append(
                    EzTimeSubsensor(name=subsensor, iterations=iterations)
                )
            channels.append(EzTimeChannel(name=channel, subsensors=subsensors))
        record = EzTimeRecord(
            test_id=test_id,
            card_id=str(card_id),
            sensor_pos=int(sensor_pos),
            run_id=run_id,
            card_tag=card_tag,
            sensor_tag=sensor_tag,
            timestamp=record_timestamp,
            channels=channels,
        )
        try:
            print(
                f"Processing EzTime record {record.test_id}/{record.card_id}-"
                f"{record.sensor_pos} {record.run_id}"
            )
            r = requests.post(f"{url}/api/tests/eztime", data=record.json())
            r.raise_for_status()
            print(r.text)
        except requests.exceptions.HTTPError as e:
            sys.stderr.write(f"{e}\n{e.response.text}\n")
            sys.exit(1)


# ----------------------------------------
# Execution File Reader
def ReadExecution(tSub=os.getcwd()):
    ts = tSub.split('\\')[-1]
    x = f'{tSub}\\{ts}_EXECUTION.txt'

    labels = ['DOSE_ITERATION', 'LEVELS', 'LEVELS_ORDER']
    lab = {}

    # populate
    with open(x, 'r') as f:
        for line in f:
            s = line.strip('\n').replace(' ', '').split(',')
            if s[0] in labels:
                lab[s[0]] = np.array(s[1:])

    # get the order of levels for the bar plot
    # check for typos
    ws = 'Undefined level in LEVELS_ORDER of EXECUTION log'
    check = all([item in lab['LEVELS'] for item in lab['LEVELS_ORDER']])
    assert bool(check) is True, ws
    lab['LEVELS_ORDER'] = np.array([
        np.where(i == lab['LEVELS'])[0][0] for i in lab['LEVELS_ORDER']])

    # set array types correctly
    lab['DOSE_ITERATION'] = lab['DOSE_ITERATION'].astype(int)

    return lab


# ----------------------------------------
# block of data pre-processing blocks
def AnnotateDf(A, lab):
    """
    Puts the execution info into the dataframe
    """
    def LevelStr(s) -> (str, str):
        def ParseLevel(s) -> (float, int):
            # with a level string, parse out it's levels
            # outputs (oxygen, glucose)
            x0, x1 = lvl.split('@')
            if '%' in x0:
                ox = int(x0[:-1])
                gl = int(x1[:-5])
            elif '%' in x1:
                ox = int(x1[:-1])
                gl = int(x0[:-5])
            return ox, gl

        ox, gl = ParseLevel(s)
        nomu = f"{int(ox):02d}PC{ox % 1:01d}"
        nomu += f"_{gl:03d}MG"
        return ox, gl, nomu

    lab['DOSE_ITERATION'] = np.append(lab['DOSE_ITERATION'] - 1, [A.last])

    # - - - - - - - - - -
    # dose nominals
    A.df['Dosing'] = -1
    for i in lab['LEVELS_ORDER']:
        it = lab['DOSE_ITERATION'][i]
        A.df.loc[A.df['I'] == it, 'Dosing'] = i

    # - - - - - - - - - -
    # set up dosing col
    # iterate through levels and fill in nominal values
    A.df['nomO2%'] = np.nan
    A.df['nomGlu'] = np.nan
    A.df['nomu'] = np.nan

    old = -1
    # this leaves incomplete iterations as NaNs, but that is fine.
    # We don't want to count those.
    for i, lvl in zip(lab['DOSE_ITERATION'], lab['LEVELS']):
        mask = ((old < A.df['I']) & (A.df['I'] <= i))
        old = i

        lvl = LevelStr(lvl)
        A.df.loc[mask, 'nomO2%'] = lvl[0]
        A.df.loc[mask, 'nomGlu'] = lvl[1]
        A.df.loc[mask, 'nomu'] = lvl[2]

    # - - - - - - - - - -
    # add in test and run IDs
    direct = os.getcwd()
    direct = direct.split('\\')
    ezid = 'EZ_000'
    ezrun = 'EZRUN_000000'
    for i in direct:
        if 'EZ_' in i and 'Data' not in i:
            ezid = i
        if 'EZRUN_' in i:
            ezrun = i

    A.df['ID_Test'] = ezid
    A.df['ID_Run'] = ezrun

    # - - - - - - - - - -
    # add in nominal o2
    # TODO

    # - - - - - - - - - -
    # order the columns
    order = [
        'ID_Test', 'ID_Run',
        'Sensor ID', 'Card', 'Sensor', 'Subsensor', 'Sensor N', 'Ch',
        'tag_sensor', 'tag_card', 'tag_u',
        'I', 'Dosing', 'nomO2%', 'nomGlu', 'nomu',
        'Timestamp', 'A1', 't1', 'B', 'C', 'Res'
    ]
    A.df = A.df[order]

    return A


# ----------------------------------------
# plot funs
class Plots_TimeSeries:
    """
    Class for plotting time series graphs. Pass it the EZID, which is set as
    the title of the plot.

    To plot an individual dataset, call self.PlotDat(sensor_name).

    Why is it a class? Because each plot has common elements, namely the FMS
    logs during the test period, dose lines, and labels. These are plotted
    only once, and then copied for ech sensor.
    """

    def __init__(self, ezid, A, clear=True, figsize=FIG_SIZE):
        # - - - - - - - - - -
        # internal inits
        self.ezid = ezid
        self.A = A
        self.df = self.A.df

        # channel color key
        self.c = {
            'Hpo': "tab:blue", 'Eug': "tab:orange",
            'Hpr': "tab:green", 'Ref': "tab:red"}

        # - - - - - - - - - -
        # init plots
        if clear:
            plt.close('all')
        self.fig = plt.figure(figsize=figsize)
        # setting sharex fixes the widths of the two plots
        grid = self.fig.add_gridspec(ncols=2).subplots(sharex=True)

        # init the grid
        self.tau = self.fig.add_subplot(grid[0])
        self.amp = self.fig.add_subplot(grid[1])
        self.tmp = self.amp.twinx()
        self.oxy = self.amp.twinx()
        self.tmp.spines['right'].set_position(('axes', 1.15))

        # - - - - - - - - - -
        # common plot functions
        # plot, temp and O2 logs, which is common to all timeseries plots
        self.PlotLogs(*lta.LogReadingUtils(self.A.df['Timestamp']).RDOT())
        # Adjust timestamps to elapsed min
        self.A.df['Timestamp'] -= min(self.A.df['Timestamp'])
        self.A.df['Timestamp'] /= 60

        # premtively format lifetime and tau axes
        self.Preform()
        self.WaterMarks()
        self.DoseMarkers()

    def PlotLogs(self, logup, logdown) -> None:
        # - - - - - - - - - -
        # upstream
        if not logup.empty:
            self.tmp.plot(
                logup['time_utc'] / 60, logup['temperature'],
                '-.', c='magenta', alpha=0.7, label='T \u2191')
            self.oxy.plot(
                logup['time_utc'] / 60, logup['oxygen'] / 0.31,
                '--', c='lightcoral', alpha=0.7, label='O2 \u2191')
        # downstream
        if not logdown.empty:
            self.tmp.plot(
                logdown['time_utc'] / 60, logdown['temperature'],
                '-.', c='purple', alpha=0.7, label='T \u2193')
            self.oxy.plot(
                logdown['time_utc'] / 60, logdown['oxygen'] / 0.31,
                '--', c='firebrick', alpha=0.7, label='O2 \u2193')

        # - - - - - - - - - -
        # glams
        self.tmp.set_yticks(range(15, 50, 5))
        self.tmp.spines['right'].set_color('purple')
        self.tmp.tick_params(axis='y', colors='purple')
        self.tmp.set_ylabel('Temperature (\u00b0C)', c='purple')

        self.oxy.set_ylim([0, 6])
        self.oxy.spines['right'].set_color('firebrick')
        self.oxy.tick_params(axis='y', colors='firebrick')
        self.oxy.set_ylabel('Oxygen (%)', c='firebrick')

        # legend
        hndT, lblT = self.tmp.get_legend_handles_labels()
        hndO, lblO = self.oxy.get_legend_handles_labels()
        hand = hndT + hndO
        labl = lblT + lblO

        self.oxy.legend(hand, labl, ncol=2, loc='upper left')

    def WaterMarks(self, ps=(0.02, 0.90)) -> None:
        # version and execution watermarks
        vLTA = lta.LTA_EXE.split('_')[-1][:-4]

        s = VERSION
        s += f'\nLTA v{vLTA}; {lta.VERSION}\n'
        s += str(datetime.now())[:-7]
        self.fig.text(*ps, s, c='gray', alpha=0.6)

    def Preform(self) -> None:
        # - - - - - - - - - -
        # axis labels
        self.tau.set_xlabel('Time (min)')
        self.amp.set_xlabel('Time (min)')

        self.tau.set_ylabel(r'$\tau$ ($\mu$s)')
        self.amp.set_ylabel(r'Amplitude ($10^3$ counts)')

        # - - - - - - - - - -
        # axis lims
        # tau range depends on OSP formulation, so this stays fixed
        self.tau.set_ylim([0, self.A.tMax])
        # amp is in thousands
        self.amp.set_ylim([0, self.A.aMax / 1000])

    def DoseMarkers(self) -> None:
        # find the timestamps of the dosing iterations
        dosing = lta.SetSort(self.df['Dosing'])[:-1]

        x = np.empty_like(dosing)
        y = np.empty_like(dosing, dtype='<U16')

        for i, di in enumerate(dosing):
            x[i] = max(self.df[self.df['Dosing'] == di]['Timestamp'])
            y[i] = self.df[self.df['Dosing'] == di + 1]['nomu'].values[0]

        x = x[1:]
        yy = [f'{y[i]} \u2192 {y[i + 1]}' for i in range(len(x))]

        # offset the text from the line
        xoff = np.diff(self.tau.get_xlim())[0] * 0.02
        for xi, yi in zip(x, yy):
            self.tau.axvline(x=xi, c='k', ls='-', alpha=0.5)
            self.amp.axvline(x=xi, c='k', ls='-', alpha=0.5)

            self.tau.text(
                xi + xoff, self.A.tMax * 0.95, yi,
                rotation='vertical', va='top', c='k', alpha=0.8, fontsize=11)

    def PlotDat(self, sensor, save=None) -> None:
        # the hack-ey deep copy of matplotlib figs and axes
        fig, amp, tau = pickle.loads(
            pickle.dumps((self.fig, self.amp, self.tau)))

        # - - - - - - - - - -
        # plot loop
        for ch in lta.CH:
            # define scan list
            m = (self.A.df['Sensor'] == sensor) & (self.A.df['Ch'] == ch)
            dfi = self.A.df[m]
            if dfi.empty:
                continue

            # get x (time) axis
            x = dfi['Timestamp'].values

            # get y (tau, amp) axis
            yt = dfi['t1'].values
            ya = dfi['A1'].values / 1000

            # plot
            tau.plot(x, yt, ls='-', marker='.', c=self.c[ch], label=ch)
            amp.plot(x, ya, ls='-', marker='.', c=self.c[ch])

        # - - - - - - - - - -
        # plot glams
        hand, labl = tau.get_legend_handles_labels()
        fig.legend(
            hand, labl, ncol=len(hand),
            loc='center', bbox_to_anchor=(0.775, 0.94), frameon=False)

        fig.suptitle(f'{self.ezid}\n{sensor}')
        fig.tight_layout()
        if save is not None:
            fig.savefig(f'{save}.png')
        else:
            fig.show()


def PlotBar(A, ezid, lvls=None, save=None, clear=True) -> None:
    """
    Takes the data found in an AnalysisUtils instance and plots the bar charts.

    ezid: is actually just the title, and does not need to be the ID.

    lvls: Define a custom order to plot doses. Default is to go in order and
        cycle though all of them

    save: Saves the plots as this. Annotates tau and amp for each set of plots.
        If none, then does plt.show() instead

    clear: does plt.close('all'). Generally a good idea
    """

    # - - - - - - - - - -
    # fig inits
    if clear:
        plt.close('all')

    res = 0.235

    figt = plt.figure(figsize=FIG_SIZE)
    gridt = figt.add_gridspec().subplots()
    figa = plt.figure(figsize=FIG_SIZE)
    grida = figa.add_gridspec().subplots()

    def BarX(nch, i):
        # bar graph spacings
        if nch == 3:
            x = [-res + i, i, res + i]
        elif nch == 4:
            x = [-res * 1.5 + i, -res / 2 + i, res / 2 + i, res * 1.5 + i]
        elif nch == 1:
            x = [i]
        return x

    def WaterMarks(fig, ps=(0.02, 0.90)) -> None:
        # version and execution watermarks
        vLTA = lta.LTA_EXE.split('_')[-1][:-4]

        s = VERSION
        s += f'\nLTA v{vLTA}; {lta.VERSION}\n'
        s += str(datetime.now())[:-7]
        fig.text(*ps, s, c='gray', alpha=0.6)

    tcard = lta.SetSort(A.df['tag_card'])
    tsens = lta.SetSort(A.df['tag_sensor'])

    # - - - - - - - - - -
    # data inits
    llab = []
    if lvls is None:
        nlvl = len(lta.SetSort(A.df['Dosing'])) - 1  # number of levels
        lvls = range(nlvl)
    else:
        nlvl = len(lvls)

    for i in lvls:  # get dose levels in order
        llab += [A.df[A.df['Dosing'] == i]['nomu'].values[0]]

    ns = len(A.df[A.df['Dosing'] == 0])  # number of tested sensors + channels

    yt = np.zeros([nlvl, ns])
    ya = np.zeros_like(yt)

    # - - - - - - - - - -
    # get y axis by loping over config tags
    for ii, li in enumerate(lvls):
        dfl = A.df[A.df['Dosing'] == li]  # subsect current level

        s0 = 0
        for tc in tcard:
            for ts in tsens:
                m = (dfl['tag_card'] == tc) & (dfl['tag_sensor'] == ts)
                s1 = sum(m)
                if s1 == 0:
                    # empty dataset
                    continue

                yt[ii, s0:s1 + s0] = dfl[m]['t1'].values
                ya[ii, s0:s1 + s0] = dfl[m]['A1'].values / 1000
                s0 += s1

    # - - - - - - - - - -
    # determine the x locations of each bar
    # could this be in the above loop? yes, but this is conceptually simpler
    x = []  # x positions of bars
    xlab = []  # the label associated with the group of bar charts
    xlabx = []  # center of a set of bars

    xsen = []  # group position
    xcard = []

    xtsen = []  # group tag in order of plot
    xtcard = []

    dfl = A.df[A.df['Dosing'] == 0]

    ii = 0
    for tc in tcard:
        for ts in tsens:
            m = (dfl['tag_card'] == tc) & (dfl['tag_sensor'] == ts)
            if sum(m) == 0:
                # empty dataset
                continue

            xtsen += [ts]
            # get list of sensors
            xl = [i.split('_')[0][:-4] for i in list(dfl[m].index)]
            sens = lta.SetSort(xl)

            # loop over sensors and get how many channels for said sensor
            for s in sens:
                nch = sum([s in i for i in xl])
                x += BarX(nch, ii)
                xlabx += [ii]
                ii += 1
            xlab += list(sens)

            # looped over sensors in a card
            xsen += [ii]
            ii += 1

        # looped over card
        xtcard += [tc]
        xcard += [ii - 1]

    # - - - - - - - - - -
    # plot baseline
    x = np.array(x)
    gridt.plot(
        [x - res / 2, x + res / 2], [yt[0], yt[0]],
        '-r', label=llab[0])
    grida.plot(
        [x - res / 2, x + res / 2], [ya[0], ya[0]],
        '-r', label=llab[0])

    # plot the bars
    for i in range(nlvl - 1):
        gridt.bar(
            x, yt[i + 1] - yt[i],
            bottom=yt[i], width=res, ec='k', label=llab[i + 1])
        grida.bar(
            x, ya[i + 1] - ya[i],
            bottom=ya[i], width=res, ec='k', label=llab[i + 1])

    # - - - - - - - - - -
    # legend
    hand, labl = gridt.get_legend_handles_labels()
    figt.legend(
        hand[-nlvl:], labl[-nlvl:],
        loc='center', bbox_to_anchor=(0.75, 0.94), ncol=3, frameon=False)

    hand, labl = grida.get_legend_handles_labels()
    figa.legend(
        hand[-nlvl:], labl[-nlvl:],
        loc='center', bbox_to_anchor=(0.75, 0.94), ncol=3, frameon=False)

    # - - - - - - - - - -
    # plot group delineators after other artists go down
    xsen = xsen
    xcard = xcard
    for i in xsen[:-1]:
        if i in xcard[:-1]:
            gridt.axvline(i, ls='-', c='r', ymax=1.05, clip_on=False)
            grida.axvline(i, ls='-', c='r', ymax=1.05, clip_on=False)
        else:
            gridt.axvline(i, ls='--', c='r')
            grida.axvline(i, ls='--', c='r')

    xsen = np.array([-1] + xsen)
    xcard = np.array([-1] + xcard)

    xsen = (xsen[1:] + xsen[:-1]) / 2
    xcard = (xcard[1:] + xcard[:-1]) / 2

    for i, ix in enumerate(xsen):
        gridt.text(
            ix, A.tMax * 0.03, xtsen[i],
            c='r', fontsize=12, va='center', ha='center')
        grida.text(
            ix, A.aMax * 0.03 / 1000, xtsen[i],
            c='r', fontsize=12, va='center', ha='center')
    for i, ix in enumerate(xcard):
        gridt.text(
            ix, A.tMax * 1.03, xtcard[i],
            c='r', fontsize=12, va='center', ha='center')
        grida.text(
            ix, A.aMax * 1.03 / 1000, xtcard[i],
            c='r', fontsize=12, va='center', ha='center')

    # - - - - - - - - - -
    # other glams
    gridt.set_xticks(xlabx)
    gridt.set_xticklabels(xlab, rotation=75, size=8)
    grida.set_xticks(xlabx)
    grida.set_xticklabels(xlab, rotation=75, size=8)

    gridt.set_xlim([-1, ii - 1])
    grida.set_xlim([-1, ii - 1])

    WaterMarks(figt)
    WaterMarks(figa)

    gridt.set_ylim([0, A.tMax])
    gridt.set_ylabel(r'$\tau$ ($\mu$s)')
    figt.suptitle(f'{ezid}\nHpo->Eug->Hpr', x=0.4, y=0.97)

    grida.set_ylim([0, A.aMax / 1000])
    grida.set_ylabel(r'Amplitude ($10^3$ counts)')
    figa.suptitle(f'{ezid}\nHpo->Eug->Hpr', x=0.4, y=0.97)

    adj = {
        'top': 0.855,
        'bottom': 0.16,
        'left': 0.065,
        'right': 0.985,
        'hspace': 0.2,
        'wspace': 0.2}
    figt.subplots_adjust(**adj)
    figa.subplots_adjust(**adj)

    if save is None:
        figt.show()
        figa.show()
    else:
        figt.savefig(f'{save}-tau.png')
        figa.savefig(f'{save}-amp.png')


# ----------------------------------------
# __main__

if args.f is None:
    args.f = [os.getcwd()]

for runFolder in args.f:
    # - - - - - - - - - -
    # Get data and logs
    T = lta.TestUtils(runFolder)
    A = lta.AnalysisUtils(runFolder, sat=5000)
    L = lta.LogReadingUtils(A.df['Timestamp'].values)

    # annotate with execution data, but don't if already fully processed
    if '.csv' not in runFolder:
        A = AnnotateDf(A, ReadExecution(runFolder))

    # - - - - - - - - - -
    # file io stuff
    if args.p:
        sDir = T.dfDir
        sDirTime = f'{T.dfDir}\\TimeSeries\\{T.ezid}'
        sDirBar = f'{T.dfDir}\\Bars'

        # init EZ_ folder in NAS
        os.makedirs(f'N:\\EZ_DATA\\{T.ezid}', exist_ok=True)

        # save annotated dataset
        lta.SaveDF(A.df, T.ezid, T.ezrun, A.model, sDir=f'{sDir}\\Data')

        # this is now the CSV
        csv_file = f'{sDir}\\Data\\DATA-{T.ezid}-{T.ezrun}-{A.model}.csv'

        # - - - - - - - - - -
        # upload to hermes
        run_id, timestamp = get_metadata(csv_file)
        df = eztime_csv_to_df(csv_file)
        eztime_df_to_db(
            df=df, run_id=run_id, record_timestamp=timestamp, url=DS_URL
        )

    else:
        sDirTime = f'TimeSeries'
        sDirBar = f'Bars'

    # make sure relevant folders exist
    os.makedirs(sDirTime, exist_ok=True)
    os.makedirs(sDirBar, exist_ok=True)

    # - - - - - - - - - -
    # plots
    P = Plots_TimeSeries(T.ezid, A)

    for ii, i in enumerate(A.sensors):
        print(f'Drawing {i}; ({ii + 1} of {len(A.sensors)})')
        P.PlotDat(i, save=f'{sDirTime}\\TIME-{i}')
    PlotBar(A, T.ezid, save=f'{sDirBar}\\BAR-{T.ezid}-{T.ezrun}')

if args.p:
    donezo = '\n'\
        'Plots and data set have been exported to NAS.\n Please zip up the '\
        'RawData folder, and migrate\n it manually, along with ScanAreaImages'
    print(donezo)
